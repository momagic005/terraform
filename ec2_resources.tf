locals {
  time = formatdate("DD MMM YYYY hh:mm ZZZ", timestamp())
  #ec2InstanceType = "${var.deploymentType == "DEV" ? var.instancetypes[0] : var.instancetypes[1]}"
  ec2InstanceType = lookup(var.instancetypes, terraform.workspace)
}

resource "aws_eip_association" "myeip_assoc" {
  instance_id   = aws_instance.myec2.id
  allocation_id = aws_eip.myeip.id
  depends_on    = [aws_instance.myec2]
}

resource "aws_network_interface_sg_attachment" "myec2sg" {
  security_group_id    = aws_security_group.mysg.id
  network_interface_id = aws_instance.myec2.primary_network_interface_id
  depends_on           = [aws_instance.myec2]
}


resource "aws_instance" "myec2" {

  ami           = lookup(var.ami, var.aws_region)
  instance_type = local.ec2InstanceType
  key_name      = "momagic_oregon"

  tags = {
    Name = "MyTerraDemo"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.myec2.private_ip} >> myec2_ips.txt"
  }
  provisioner "local-exec" {
    when    = destroy
    command = "echo 'Destroy-time provisioner' >> status.txt"
  }

}

resource "null_resource" "preparation" {

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file("/Users/movaswan/Documents/Cisco/keys/momagic_cc/momagic_oregon.pem")
    host        = aws_eip.myeip.public_ip
    #timeout     = "360s"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",
      "sudo yum -y install httpd",
      "sudo service httpd start",
      "sudo chkconfig httpd on"
    ]
  }

}
