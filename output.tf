output "timestamp" {
  value = local.time
}
output "myec2" {
  value = aws_instance.myec2.public_ip
}

output "mysg" {
  value = aws_security_group.mysg.id
}
