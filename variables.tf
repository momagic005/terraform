variable aws_access_key {}
variable aws_secret_key {}
variable aws_region {
  type    = string
  default = "us-west-2"
}
/*variable instancetypes {
  type    = list
  default = ["t2.micro", "t2.medium"]
}*/

variable "instancetypes" {
  type = map
  default =  {
    default = "t2.nano"
    dev = "t2.micro"
    prod = "t2.large"
  }
}
variable deploymentType {
  type    = string
  default = "PROD"
}
variable cidrblock {
  type    = string
  default = "128.107.241.162/32"
}
variable "ami" {
  type = map
  default = {
    "us-east-1"  = "ami-0323c3dd2da7fb37d"
    "us-west-2"  = "ami-0d6621c01e8c2de2c"
    "ap-south-1" = "ami-0470e33cd681b2476"
  }
}

variable "sg_ports" {
  type    = list(number)
  default = [22]
}

variable "sg_ports_any" {
  type    = list(number)
  default = [80, 8080]
}
