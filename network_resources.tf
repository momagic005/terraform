resource "aws_security_group" "mysg" {
  name       = "myhttpdsg"
  depends_on = [aws_instance.myec2]
  dynamic "ingress" {
    for_each = var.sg_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = [var.cidrblock]
    }
  }
  dynamic "ingress" {
    for_each = var.sg_ports_any
    iterator = any_port
    content {
      from_port   = any_port.value
      to_port     = any_port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

}
resource aws_eip "myeip" {
  vpc        = "true"
  depends_on = [aws_instance.myec2]
}
