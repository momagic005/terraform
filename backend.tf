terraform {
  backend "s3" {
    bucket = "moterra"
    key    = "finalterra.tfstate"
    region = "us-west-2"
    dynamodb_table = "S3_TFSTATES"
  }
}
